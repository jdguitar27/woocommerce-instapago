<?php

function consultar_pago($parametros)
{
    $url = add_query_arg($parametros, 'https://api.instapago.com/payment');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);

    $response = new stdClass();
    $response = json_decode($server_output);
    return $response;
}

function realizar_pago($parametros)
{

    $url = 'https://api.instapago.com/payment';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parametros));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);

    $response = new stdClass();
    $response = json_decode($server_output);
    return $response;
}