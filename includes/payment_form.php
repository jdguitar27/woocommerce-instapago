<script>
    function testOnlyNumbers(str) {
        return /\d/.test(str);
    }

    function testValidInteger(str) {
        return /^[1-9](\d*)$/.test(str);
    }

    function restrictInputCI(e) {
        const input = String.fromCharCode(e.which);
        const text = e.currentTarget.value + input;
        if (!testOnlyNumbers(input))
            e.preventDefault();

        if (!testValidInteger(text) || text.length > 8)
            e.preventDefault();
    }

    function restrictInputCardNumber(e) {
        const input = String.fromCharCode(e.which);
        const text = e.currentTarget.value + input;
        if (!testOnlyNumbers(input) || text.length > 16)
            e.preventDefault();
    }

    function restrictInputCVC(e) {
        const input = String.fromCharCode(e.which);
        const text = e.currentTarget.value + input;
        if (!testOnlyNumbers(input) || text.length > 3)
            e.preventDefault();
    }

    function restrictInputToOnlyLetters(e) {
        const input = String.fromCharCode(e.which);

        if (!(/[a-zA-Z\s]/.test(input)))
            e.preventDefault();
    }

    function restrictInputExpDate(e) {
        const input = String.fromCharCode(e.which);
        const text = e.currentTarget.value + input;

        if (e.currentTarget.value.length >= 7)
            e.preventDefault();

        if (e.currentTarget.value.length < 2 || e.currentTarget.value.length > 2) {
            if (!(/\d/.test(input)))
                e.preventDefault();
        } else {
            e.currentTarget.value += '/';
            e.preventDefault();
        }
    }

    function enablePlaceOrderButton () {
        $('#place_order').attr('disabled', false);
    }

    jQuery(function ($) {

        var checkout_form = $('form.woocommerce-checkout');
        checkout_form.on('checkout_place_order', function () {
            var fecha = /(\d{2})\/(\d{4})/.exec(this.elements['card-exp-date'].value);
            this.elements['card-exp-date-format'].value = fecha[2] + '-' + fecha[1];
        });

    });
</script>
<?php
// I will echo() the form
echo '<fieldset id="wc-' . esc_attr($this->id) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent;">';

// Add this action hook if you want your custom payment gateway to support it
do_action('woocommerce_credit_card_form_start', $this->id);

// Use uniques IDs
echo '
    <div class="form-row form-row-wide">
        <label for="card-holder-name">' . __('Card Holder', 'woocommerce-instapago') . ' <span class="required">*</span></label>
        <input class="input-text" id="card-holder-name" name="card-holder-name" onkeypress="restrictInputToOnlyLetters(event)" type="text" autocomplete="off">
    </div>
    <div class="form-row form-row-wide">
        <label for="card-holder-ci">' . __('Card holder ID', 'woocommerce-instapago') . ' <span class="required">*</span></label>
        <input class="input-text" id="card-holder-ci" name="card-holder-ci" type="text" onkeypress="restrictInputCI(event)" autocomplete="off">
    </div>
    <div class="form-row form-row-wide">
        <label for="card-number">' . __('Card Number', 'woocommerce-instapago') . ' <span class="required">*</span></label>
        <input class="input-text" id="card-number" name="card-number" type="text" onkeypress="restrictInputCardNumber(event)" autocomplete="off">
    </div>
    <div class="form-row">
        <label for="card-cvc">' . __('Card Code (CVV)', 'woocommerce-instapago') . ' <span class="required">*</span></label>
        <input class="input-text" id="card-cvc" name="card-cvc" type="password" onkeypress="restrictInputCVC(event)" autocomplete="off">
    </div>
    <div class="form-row form-row-first">
        <label for="card-exp-date">' . __('Expiry Date', 'woocommerce-instapago') . ' <span class="required">*</span></label>
        <input class="input-text" id="card-exp-date" name="card-exp-date" type="text" onkeypress="restrictInputExpDate(event)" autocomplete="off" placeholder="MM / ' . __('YYYY', 'woocommerce-instapago') . '">
        <input id="card-exp-date-format" name="card-exp-date-format" type="hidden">
    </div>
    <div style="text-align: center;" class="form-row form-row-wide">
        <p>Esta transacción será procesada de forma segura gracias a la plataforma de:</p>
        <img src="' . plugins_url( 'assets/images/logo-instapago-minimo.png', dirname(__FILE__ )) .'">
    </div>
    <div class="clear"></div>';

do_action('woocommerce_credit_card_form_end', $this->id);

echo '<div class="clear"></div></fieldset>';