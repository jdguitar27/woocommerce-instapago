��    $      <  5   \      0     1     A     M      Y     z     �     �     �     �     �     �     �     �             =     ;   Z     �     �     �  	   �  !   �  8   �  #   6  2   Z  +   �  &   �  =   �  (     "   G  #   j  B   �  <   �            }       �     �     �  4   �     	     $	     8	     E	  	   R	     \	     z	     �	     �	     �	     �	     �	  =   �	  ?   8
     x
     �
     �
  J   �
  ;     =   @  ?   ~  2   �  I   �  D   ;  H   �  2   �  J   �  9   G  4   �     �     �                                                             $                       "   !                          #          
                        	                                 Card Code (CVV) Card Holder Card Number Card expiry date already expired Card holder ID Credit Card Description Disable Enable Enable Instapago Gateway Enable Test Mode Expiry Date Instapago Payment Gateway Live Key Id Live Public Key Pay with your credit card via our super-cool payment gateway. Place the payment gateway in test mode using test API keys. Recive payments with Instapago Test Key Id Test Public Key Test mode The "Card Code" field is required The "Card Code" field must contain at least 3 characters The "Card Number" field is required The "Card Number" field must contain 16 characters The "Card expiration date" field is invalid The "Card holder ID" field is required The "Card holder ID" field must contain at least 6 characters The "Card holder name" field is required The "Expiry Date" field is invalid The "Expiry Date" field is required This controls the description which the user sees during checkout. This controls the title which the user sees during checkout. Title YYYY Project-Id-Version: 
POT-Creation-Date: 2019-05-28 21:14-0430
PO-Revision-Date: 2019-05-28 21:21-0430
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
 Código de seguridad (CVC) Titular de la tarjeta Número de tarjeta La fecha de vencimiento de la tarjeta ya ha caducado Cédula del titular Tarjeta de Crédito Descripción Deshabilitar Habilitar Habilitar pagos con Instapago Habilitar Modo de Prueba Fecha de vencimiento Pagos con Instapago ID de la Llave Llave Pública Paga con tu tarjeta de crédito Cambia a modo de pruebas para usar llaves de prueba de la API Recibe tus pagos de tarjetas de crédito a través de Instapago ID de la Llave de Prueba Llave Pública de Prueba Modo de prueba El código de seguridad de la tarjeta es obligatorio para procesar el pago El código de la tarjeta debe contener mínimo 3 caracteres El número de la tarjeta es obligatorio para procesar el pago El número de la tarjeta debe contener 16 caracteres numéricos La fecha de vencimiento de la tarjeta es inválida La cédula del titular de la tarjeta es obligatoria para procesar el pago La cédula del titular debe contener mínimo 6 caracteres numéricos El nombre del titular de la tarjeta es obligatorio para procesar el pago La fecha de vencimiento de la tarjeta es inválida La fecha de vencimiento de la tarjeta es obligatoria para procesar el pago Descripción que el usuario ve durante el proceso de pago Título que el usuario ve durante el proceso de pago Título AAAA 