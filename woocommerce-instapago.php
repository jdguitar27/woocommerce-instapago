<?php

/*
Plugin Name: Woocommerce Instapago Payment Gateway
Description: Permite hacer pagos con tarjetas de crédito en Bolívares
Version: 0.1
Author: José Durán
Text Domain: woocommerce-instapago
*/

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

//Verifica si el usuario está logueado o no para habilitar el formulario de login
add_action('woocommerce_before_checkout_form', 'check_if_logged_in');
function check_if_logged_in()
{
    if (!is_user_logged_in()) {
        echo "
            <script>
                window.onload = () => {
                    const login = document.getElementsByClassName('woocommerce-form-login');
                    if(login.length > 0)
                        login[0].style = 'display: block !important;';
                }
            </script>";
    }
}

function woocommerce_instapago_load_plugin_textdomain()
{
    load_plugin_textdomain('woocommerce-instapago', FALSE, dirname(plugin_basename(__FILE__)) . '/languages/');
}
add_action('plugins_loaded', 'woocommerce_instapago_load_plugin_textdomain');

//Agrega el comprobante de pago a la página de finalización del pedido
add_action('woocommerce_thankyou', function ($order) {
    if (isset($_GET['wci_payment_id'])) {
        require_once('instapago-utils.php');
        $gateway = new WC_Instapago_Gateway;

        echo '<h2>Datos de pago</h2><br>
              <p>Tu pago se ha realizado con éxito!<br><br>
                  Id: ' . $_GET['wci_payment_id'] . '<br>
                  Referencia: ' . $_GET['payment_ref'] . ' 
              </p>';

        $datos_pago = array(
            "KeyId" => $gateway->key_id,
            "PublicKeyId" => $gateway->public_key,
            "Id" => $_GET['wci_payment_id']);
        $response = consultar_pago($datos_pago);
        if ($response->success)
            echo '<h2>Comprobante de pago</h2><br>' . htmlspecialchars_decode($response->voucher);
        else
            echo 'No se pudo cargar el comprobante de pago. Recargue la página';
    }
}, 10, 1);

/*
 * Registra la clase como payment gateway
 */
add_filter('woocommerce_payment_gateways', 'instapago_add_gateway_class');
function instapago_add_gateway_class($gateways)
{
    $gateways[] = 'WC_Instapago_Gateway'; // Class name
    return $gateways;
}

/*
 * Carga en plugin en Instapago
 */
add_action('plugins_loaded', 'instapago_init_gateway_class');
function instapago_init_gateway_class()
{

    class WC_Instapago_Gateway extends WC_Payment_Gateway
    {

        public function __construct()
        {
            $this->id = "wc-instapago";
            $this->icon = apply_filters( 'woocommerce_gateway_icon', plugins_url( 'assets/images/logo-instapago-minimo.png', __FILE__ ) ); // URL del icono que se mostrará al lado del nombre del Plugin
            $this->has_fields = true; // Habilita el uso de tarjetas de crédito
            $this->method_title = __('Instapago Payment Gateway', 'woocommerce-instapago');  // Título del método de pago
            $this->method_description = __('Recive payments with Instapago', 'woocommerce-instapago'); // Descripción para la ventana de administrador

            $this->supports = array(
                'products',
                'default_credit_card_form'
            );

            // Method with all the options fields
            $this->init_form_fields();

            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->enabled = $this->get_option('enabled');
            $this->testmode = 'yes' === $this->get_option('testmode');
            $this->key_id = $this->testmode ? $this->get_option('test_key_id') : $this->get_option('key_id');
            $this->public_key = $this->testmode ? $this->get_option('test_public_key') : $this->get_option('public_key');

            // This action hook saves the settings
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

            // Custom javascript files
            add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
        }

        /**
         * Plugin admin options
         */
        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable', 'woocommerce-instapago') . __('Disable', 'woocommerce-instapago'),
                    'label' => __('Enable Instapago Gateway', 'woocommerce-instapago'),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'title' => array(
                    'title' => __('Title', 'woocommerce-instapago'),
                    'type' => 'text',
                    'description' => __('This controls the title which the user sees during checkout.', 'woocommerce-instapago'),
                    'default' => __('Credit Card', 'woocommerce-instapago'),
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Description', 'woocommerce-instapago'),
                    'type' => 'textarea',
                    'description' => __('This controls the description which the user sees during checkout.', 'woocommerce-instapago'),
                    'default' => __('Pay with your credit card via our super-cool payment gateway.', 'woocommerce-instapago'),
                ),
                'testmode' => array(
                    'title' => __('Test mode', 'woocommerce-instapago'),
                    'label' => __('Enable Test Mode', 'woocommerce-instapago'),
                    'type' => 'checkbox',
                    'description' => __('Place the payment gateway in test mode using test API keys.', 'woocommerce-instapago'),
                    'default' => 'yes',
                    'desc_tip' => true,
                ),
                'test_key_id' => array(
                    'title' => __('Test Key Id', 'woocommerce-instapago'),
                    'type' => 'text'
                ),
                'test_public_key' => array(
                    'title' => __('Test Public Key', 'woocommerce-instapago'),
                    'type' => 'text',
                ),
                'key_id' => array(
                    'title' => __('Live Key Id', 'woocommerce-instapago'),
                    'type' => 'text'
                ),
                'public_key' => array(
                    'title' => __('Live Public Key', 'woocommerce-instapago'),
                    'type' => 'text'
                )
            );
        }

        /**
         * Display a custom credit card form
         */
        public function payment_fields()
        {
            // Display a description before the payment form
            if ($this->description) {
                // you can instructions for test mode, I mean test card numbers etc.
                if ($this->testmode) {
                    $this->description .= '<br>' . __('TEST MODE ENABLED. In test mode, you can use the card numbers listed below', 'woocommerce-instapago') . ': <br><br>
                                         <ul style="list-style: none !important;">   
                                            <li><b>Visa</b>: 4111111111111111</li>
                                            <li><b>MasterCard</b>: 5105105105105100</li>
                                            <li><b>Sambil</b>: 8244001100110011</li>
                                         </ul>
                                            ';
                    $this->description = trim($this->description);
                }
                // display the description with <p> tags etc.
                echo wpautop(wp_kses_post($this->description));
            }

            require_once('includes/payment_form.php');
        }

        /*
         * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
         */
        public function payment_scripts()
        {
            if (!is_cart() && !is_checkout() && !isset($_GET['pay_for_order'])) {
                return;
            }

            if ($this->enabled === 'no') {
                return;
            }

            if (empty($this->key_id) || empty($this->public_key)) {
                return;
            }

            if (!$this->testmode && !is_ssl()) {
                return;
            }
            wp_register_script('woocommerce_instapago', plugins_url('woocommerce-instapago.js', __FILE__), array('jquery'));

            wp_localize_script('woocommerce_instapago', 'instapago_params', array(
                'publicKey' => $this->public_key
            ));

            wp_enqueue_script('woocommerce_instapago');
        }

        /*
          * Fields validation
         */
        public function validate_fields()
        {
            require_once('instapago-utils.php');
            if (empty($_POST['card-holder-name'])) {
                wc_add_notice(__('The "Card holder name" field is required', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (empty($_POST['card-holder-ci'])) {
                wc_add_notice(__('The "Card holder ID" field is required', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (strlen($_POST['card-holder-ci']) < 6) {
                wc_add_notice(__('The "Card holder ID" field must contain at least 6 characters', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (empty($_POST['card-number'])) {
                wc_add_notice(__('The "Card Number" field is required', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (strlen($_POST['card-number']) < 15) {
                wc_add_notice(__('The "Card Number" field must contain at least 15 characters', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (empty($_POST['card-cvc'])) {
                wc_add_notice(__('The "Card Code" field is required', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (strlen($_POST['card-cvc']) < 3) {
                wc_add_notice(__('The "Card Code" field must contain at least 3 characters', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (empty($_POST['card-exp-date'])) {
                wc_add_notice(__('The "Expiry Date" field is required', 'woocommerce-instapago'), 'error');
                return false;
            }

            if (empty($_POST['card-exp-date-format'])) {
                wc_add_notice(__('The "Expiry Date" field is invalid', 'woocommerce-instapago'), 'error');
                return false;
            }

            $actual = new DateTime();//Establece la fecha actual
            $actual = new DateTime($actual->format('Y-m'));//Establece la fecha con el mes y año actual, al primer día del mes
            try {
                $vencimiento = new DateTime($_POST['card-exp-date-format']);
            } catch (Exception $err) {
                wc_add_notice(__('The "Card expiration date" field is invalid', 'woocommerce-instapago'), 'error');
                return false;
            }

            if ($vencimiento < $actual) {
                wc_add_notice(__('Card expiry date already expired', 'woocommerce-instapago'), 'error');
                return false;
            }

            return true;
        }

        /*
         * Procesamiento del pago
         */
        public function process_payment($order_id)
        {
            require_once('instapago-utils.php');
            $order = wc_get_order($order_id);
            //Datos de la tarjeta del cliente y la API de Instapagos para realizar el pago
            $datos_cc = array(
                "KeyID" => $this->key_id,
                "PublicKeyId" => $this->public_key,
                "Amount" => $order->get_total(),
                "Description" => "Pago de pedido #" . $order->get_order_number(),
                "CardHolder" => $_POST['card-holder-name'],
                "CardHolderId" => $_POST['card-holder-ci'],
                "CardNumber" => $_POST['card-number'],
                "CVC" => $_POST['card-cvc'],
                "ExpirationDate" => $_POST['card-exp-date'],
                "StatusId" => "2");

            $response = realizar_pago($datos_cc);
            $redirect_url = $this->get_return_url($order);
            if ($response->success && $response->responsecode == '00') {
                $order->update_status('completed');
                $order->add_order_note('Transacción éxitosa desde instapago. Referencia de pago: ' . $response->reference . '<br> Id: ' . $response->id);

                $redirect_url = add_query_arg(array("wci_payment_id" => $response->id, "payment_ref" => $response->reference), $redirect_url);
            } else {
                $order->update_status('failed');
                $this->handle_bad_response($response, $order);
                $redirect_url = '/trexa/tienda';
            }

            // Remove cart
            WC()->cart->empty_cart();

            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $redirect_url
            );
        }

        private function handle_bad_response($response, $order)
        {
            switch ($response->code) {
                case '400':
                    wc_add_notice('No se pudo realizar la transacción: Error al validar los datos enviados.<br>
                                            Detalles del error: ' . $response->message, 'error');
                    $order->add_order_note('No se pudo realizar la transacción: Error al validar los datos enviados.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
                case '401':
                    wc_add_notice('No se pudo realizar la transacción: Error al comunicarse con Instapago. Vuelva a intentarlo o contacte con el administrador del sitio.', 'error');
                    $order->add_order_note('No se pudo realizar la transacción: Error de autenticación.<br>
                                            Ha ocurrido un error con las llaves utilizadas.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
                case '403':
                    wc_add_notice('No se pudo realizar la transacción: Pago rechazado por el banco.', 'error');
                    $order->add_order_note('No se pudo realizar la transacción: Pago rechazado por el banco.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
                case '500':
                    wc_add_notice('No se pudo realizar la transacción: Ha ocurrido un error al procesar la transacción. Vuelva a intentarlo o contacte con el administrador.', 'error');
                    $order->add_order_note('No se pudo realizar la transacción: Ha ocurrido un error interno del servidor de Instapago.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
                case '503':
                    wc_add_notice('No se pudo realizar la transacción: Ha ocurrido un error al procesar los datos del pago. Vuelva a intentarlo o contacte con el administrador.', 'error');
                    $order->add_order_note('No se pudo realizar la transacción: Ha ocurrido un error al procesar los parámetros de entrada.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
                default:
                    wc_add_notice('No se pudo realizar la transacción: Ha ocurrido un error. Revise su conexión a internet, vuelva a intentarlo o contacte con el administrador.', 'error');
                    $order->add_order_note('Error al procesar el pago.<br>
                                            Respuesta HTTP: ' . $response->code . '<br>
                                            Respuesta de la transacción: ' . $response->responsecode . '<br>
                                            Mensaje de error devuelto: ' . $response->message);
                    break;
            }
            $this->handle_responsecode($response->responsecode);
        }

        private function handle_responsecode($responsecode)
        {
            if (!empty($responsecode) && $responsecode != '00') {
                switch ($responsecode) {
                    case '01':
                        wc_add_notice('Llame al emisor', 'error');
                        break;
                    case '02':
                        wc_add_notice('Cédula inválida', 'error');
                        break;
                    case '03':
                        wc_add_notice('Comercio inválido', 'error');
                        break;
                    case '04':
                        wc_add_notice('Retenga y llame', 'error');
                        break;
                    case '05':
                        wc_add_notice('Transacción rechazada', 'error');
                        break;
                    case '06':
                        wc_add_notice('Error', 'error');
                        break;
                    case '07':
                        wc_add_notice('Retenga y llame', 'error');
                        break;
                    case '12':
                        wc_add_notice('Transacción inválida', 'error');
                        break;
                    case '13':
                        wc_add_notice('Monto inválido', 'error');
                        break;
                    case '14':
                        wc_add_notice('Tarjeta inválida', 'error');
                        break;
                    case '15':
                        wc_add_notice('Emisor inválido', 'error');
                        break;
                    case '19':
                        wc_add_notice('Reintente transacción', 'error');
                        break;
                    case '21':
                        wc_add_notice('Llame al emisor', 'error');
                        break;
                    case '25':
                        wc_add_notice('Reg. no localizado', 'error');
                        break;
                    case '28':
                        wc_add_notice('Archivo no disponible', 'error');
                        break;
                    case '30':
                        wc_add_notice('Error en formato', 'error');
                        break;
                    case '39':
                        wc_add_notice('No es cuenta credito', 'error');
                        break;
                    case '40':
                        wc_add_notice('Función no soportada', 'error');
                        break;
                    case '41':
                        wc_add_notice('Tarjeta extraviada', 'error');
                        break;
                    case '43':
                        wc_add_notice('Tarjeta robada', 'error');
                        break;
                    case '51':
                        wc_add_notice('Fondo insuficiente', 'error');
                        break;
                }
            }
        }
    }
}